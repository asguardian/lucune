package lucene;


import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.LowerCaseFilter;
import org.apache.lucene.analysis.StopFilter;
import org.apache.lucene.analysis.StopwordAnalyzerBase;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.WordlistLoader;
import org.apache.lucene.analysis.core.WhitespaceTokenizer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.synonym.SynonymGraphFilterFactory;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.util.FilesystemResourceLoader;
import org.apache.lucene.analysis.util.ResourceLoader;


public final class CustomAnalyser extends StopwordAnalyzerBase {

	  /** An unmodifiable set containing some common English words that are not usually useful
	  for searching.*/
	  public static final CharArraySet ENGLISH_STOP_WORDS_SET;
	  private String synonymnFile="synonyms.txt";
	  
	  static {
	    final List<String> stopWords = Arrays.asList(
	      "a", "an", "and", "are", "as", "at", "be", "but", "by",
	      "for", "if", "in", "into", "is", "it",
	      "no", "not", "of", "on", "or", "such",
	      "that", "the", "their", "then", "there", "these",
	      "they", "this", "to", "was", "will", "with"
	    );
	    final CharArraySet stopSet = new CharArraySet(stopWords, false);
	    ENGLISH_STOP_WORDS_SET = CharArraySet.unmodifiableSet(stopSet); 
	  }
	  
	  /** Default maximum allowed token length */
	  public static final int DEFAULT_MAX_TOKEN_LENGTH = 255;

	  private int maxTokenLength = DEFAULT_MAX_TOKEN_LENGTH;

	  /** An unmodifiable set containing some common English words that are usually not
	  useful for searching. */
	  public static final CharArraySet STOP_WORDS_SET = ENGLISH_STOP_WORDS_SET;

	  /** Builds an analyzer with the given stop words.
	   * @param stopWords stop words */
	  public CustomAnalyser(CharArraySet stopWords) {
	    super(stopWords);
	  }

	  /** Builds an analyzer with the default stop words ({@link #STOP_WORDS_SET}).
	   */
	  public CustomAnalyser(String synonymsFileName) {
	    this(STOP_WORDS_SET);
	    this.synonymnFile=synonymsFileName;
	  }

	  /** Builds an analyzer with the stop words from the given reader.
	   * @see WordlistLoader#getWordSet(Reader)
	   * @param stopwords Reader to read stop words from */
	  public CustomAnalyser(Reader stopwords) throws IOException {
	    this(loadStopwordSet(stopwords));
	  }

	  /**
	   * Set the max allowed token length.  Tokens larger than this will be chopped
	   * up at this token length and emitted as multiple tokens.  If you need to
	   * skip such large tokens, you could increase this max length, and then
	   * use {@code LengthFilter} to remove long tokens.  The default is
	   * {@link StandardAnalyzer#DEFAULT_MAX_TOKEN_LENGTH}.
	   */
	  public void setMaxTokenLength(int length) {
	    maxTokenLength = length;
	  }
	    
	  /** Returns the current maximum token length
	   * 
	   *  @see #setMaxTokenLength */
	  public int getMaxTokenLength() {
	    return maxTokenLength;
	  }

	  @Override
	  protected TokenStreamComponents createComponents(final String fieldName) { 
	    final StandardTokenizer src = new StandardTokenizer();
	    src.setMaxTokenLength(maxTokenLength);
	    TokenStream tok = new StandardFilter(src);
	    tok = new LowerCaseFilter(tok);
	    tok = new StopFilter(tok, stopwords);
	    try {
			SynonymGraphFilterFactory sgf=this.init();
			tok= sgf.create(tok);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    
	    
	    return new TokenStreamComponents(src, tok) {
	      @Override
	      protected void setReader(final Reader reader) {
	        // So that if maxTokenLength was changed, the change takes
	        // effect next time tokenStream is called:
	        src.setMaxTokenLength(CustomAnalyser.this.maxTokenLength);
	        super.setReader(reader);
	      }
	    };
	  }

	  @Override
	  protected TokenStream normalize(String fieldName, TokenStream in) {
	    TokenStream result = new StandardFilter(in);
	    result = new LowerCaseFilter(result);
	    return result;
	  }
	  
	  
	  public  SynonymGraphFilterFactory init() throws IOException
	  {
	    // prepare the list of parameters
	    Map<String, String> args = new HashMap<>();
	    System.out.println(synonymnFile);
	    args.put("synonyms",synonymnFile);
	    args.put("ignoreCase", Boolean.toString(true));
	    args.put("expand", Boolean.toString(true));
	    // create the SynonymGraphFilterFactory and the ResourceLoader
	    SynonymGraphFilterFactory syf = new SynonymGraphFilterFactory(args);
	    ResourceLoader rl = new FilesystemResourceLoader(Paths.get("."), CustomSynonymGraphFilter.class.getClassLoader());
	    syf.inform(rl);
	    return syf;
	  }

	  public  String applySynonyms(SynonymGraphFilterFactory localSyf, String input)
	  {
	    StringBuilder sb = new StringBuilder();
	    
	    // Specify the Whitespace Tokenizer divides text at whitespace characters    
	    try (Tokenizer wt = new WhitespaceTokenizer()) {
	      // Set the input string as Reader
	      wt.setReader(new StringReader(input));
	      try (TokenStream syn = localSyf.create(wt)) {
	        // This method has to called by a consumer before it begins consumption
	        syn.reset();
	        // Define a couple of attributes 
	        CharTermAttribute term = syn.addAttribute(CharTermAttribute.class);
	        OffsetAttribute offset = syn.addAttribute(OffsetAttribute.class);
	        // this method advance the stream to the next token
	        if (syn.incrementToken()) {
	          // read the first token
	          sb.append(term.toString());
	          // print start/end offsets and term
	          System.out.println(offset.startOffset() + " " + offset.endOffset() + " " + term.toString());
	          // again, advance the stream to the next token and prints 
	          while (syn.incrementToken()) {
	            System.out.println(offset.startOffset() + " " + offset.endOffset() + " " + term.toString());
	            sb.append(" ");
	            sb.append(term.toString());
	          }
	        }
	        // called by the consumer after the last token has been consumed
	        syn.end();
	      }
	    } catch (IOException e) {
	      e.printStackTrace();
	    }
	    System.out.println(sb.toString());
	    return sb.toString();
	  }
	}
